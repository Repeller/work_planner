﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
// what is needed for the new controller to run
using System.Linq;
//using System.Threading.Tasks;
using WorkPlanner.Model;

//using System.Collections.Generic;

// the new controller

namespace WorkPlanner.ViewModel
{
    class ViewModelAdmin : INotifyPropertyChanged
    {
        
          
        public event PropertyChangedEventHandler PropertyChanged;

        // project data // virker ikke ......
        public List<int> ProjectIds = new List<int>();
        public List<string> ProjectText = new List<string>();
		//public ObservableCollection<string> WorkerIdList = new ObservableCollection<string>();
		//public ObservableCollection<string> WorkerNameList = new ObservableCollection<string>();

	    public ObservableCollection<string> WorkerIdList { get; set; }
	    public ObservableCollection<string> WorkerNameList { get; set; }

        public ObservableCollection<Project> Projects { get; set; }

		// Create the OnPropertyChanged method to raise the event
		protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(name));
        }
        
        public ViewModelAdmin()
        {
            SingleUser tempSingleUser = SingleUser.User;
			WorkerIdList = new ObservableCollection<string>();
			WorkerNameList = new ObservableCollection<string>();
			Projects = new ObservableCollection<Project>(tempSingleUser.Projects);

            foreach (Employee employee in tempSingleUser.ListOfEmployees)
            {
	            WorkerIdList.Add(employee.UserId);
	            WorkerNameList.Add(employee.Name);
            }
        }
    }
}
