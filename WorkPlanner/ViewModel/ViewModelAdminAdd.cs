﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using WorkPlanner.Model;

namespace WorkPlanner.ViewModel
{
	class ViewModelAdminAdd
	{
		public List<Project> Projects { get; set; }
		public List<Employee> Employees { get; set; }

		private SingleUser logged = SingleUser.User;

		public ViewModelAdminAdd()
		{
			Projects = logged.Projects;
			Employees = logged.ListOfEmployees;
		}

        
	}
}
