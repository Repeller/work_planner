﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Xaml.Interactions.Core;
using WorkPlanner.Model;
using WorkPlanner.View;

namespace WorkPlanner.ViewModel
{
	class ViewModelEvents : INotifyPropertyChanged
	{

		#region PropertyChangeSupport
		public event PropertyChangedEventHandler PropertyChanged;

		// Create the OnPropertyChanged method to raise the event
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{ handler(this, new PropertyChangedEventArgs(name)); }
		}
		#endregion

		// private ObservableCollection<ScheduledEvent> _EventList;
		public ObservableCollection<ScheduledEvent> EventList { get; set; }

		public ViewModelEvents()
		{
		    EventList = new ObservableCollection<ScheduledEvent>();

            ObservableCollection<ScheduledEvent> TempEventList = new ObservableCollection<ScheduledEvent>();
            
            SingleUser MasterController = SingleUser.User;

		    ObservableCollection<ScheduledEvent> tempEvents = new ObservableCollection<ScheduledEvent>();

		    foreach (Project project in MasterController.Projects)
		    {
		        ObservableCollection<ScheduledEvent> tempScheduledEvents = project.GetEventsWithThatEmployee();
		        foreach (ScheduledEvent scheduledEvent in tempScheduledEvents)
		        {
		            TempEventList.Add(scheduledEvent);
		        }
		    }

		    EventList = TempEventList;

		}

	}
}
