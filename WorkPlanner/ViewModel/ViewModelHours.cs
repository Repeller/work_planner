﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using WorkPlanner.Model;

namespace WorkPlanner.ViewModel
{
	class ViewModelHours 
	{
		#region PropertyChangeSupport

		public event PropertyChangedEventHandler PropertyChanged;

		// Create the OnPropertyChanged method to raise the event
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}

		#endregion

		SingleUser SingleUserHours = SingleUser.User;
		// public EmployeePaydayCatalogue EmployeeTimer { get; set; }

		public ObservableCollection<Day> EmployeeTimer { get; set; }
		public ObservableCollection<TimeSpan> EmployeePauseTime { get; set; }


		public ViewModelHours()
		{
			EmployeeTimer = new ObservableCollection<Day>(SingleUserHours.EmployeePaychecks
				.PayDays[SingleUserHours.LoggedEmployee.UserId].WorkDays);
			EmployeePauseTime = new ObservableCollection<TimeSpan>();

			foreach (Day day in SingleUserHours.EmployeePaychecks.PayDays[SingleUserHours.LoggedEmployee.UserId]
				.WorkDays)
			{
				EmployeePauseTime.Add(day.PauseTime());
			}
		}
	}
}
