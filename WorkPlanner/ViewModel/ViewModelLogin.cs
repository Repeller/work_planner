﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Microsoft.Xaml.Interactions.Core;
using WorkPlanner.Model;
using WorkPlanner.View;
using Admin = WorkPlanner.View.Admin;

namespace WorkPlanner.ViewModel
{
	public class ViewModelLogin : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		// Create the OnPropertyChanged method to raise the event
		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(name));
		}

		private string _username;
		private string _password;
		private string _feedback;



		public string Username
		{
			get { return _username; }
			set
			{
				_username = value;
				OnPropertyChanged(nameof(Username));
			}
		}

		public string Password
		{
			get { return _password; }
			set
			{
				_password = value;
				OnPropertyChanged(nameof(Password));
			}
		}

		public string Feedback
		{
			get { return _feedback; }
			set
			{
				_feedback = value;
				OnPropertyChanged(nameof(Feedback));
			}
		}

		public SingleUser singleUser = SingleUser.User;

		public ViewModelLogin()
		{

		}

		/// <summary>
		/// called when the user will login. The user is only login, if the inputs are the right ones
		/// </summary>
		public void login(Frame currentFrame)
		{
			// check if the user did write something
			if (Username == null || Password == null || Username == "" || Password == "")
				Feedback = "you need to write your username and password to login";
			else
			{
				bool IsUser = singleUser.checkEmployeeInput(Username, Password);

				// check if the user used the right username and password
				if (IsUser)
				{
					Feedback = $"Hello {singleUser.LoggedEmployee.Name}, you are now online";
				    NavigateToMainPage(currentFrame);

				}
				else
				{
					Feedback = "you are using the wrong username or password, try again";
				}
			}
		}

		public void login_admin(Frame currentFrame)
		{
			// check if the admin did write something
			if (Username == null || Password == null || Username == "" || Password == "")
				Feedback = "you need to write your username and password to login";
			else
			{
				bool IsAdmin = singleUser.CheckAdminInput(Username, Password);

				// check if the user used the right username and password
				if (IsAdmin)
				{
					Feedback = $"Hello {singleUser.LoggedAdmin.Name}, you are now online";
                    NavigateToAdminPage(currentFrame);
				}
				else
				{
					Feedback = "you are using the wrong username or password, try again";
				}
			}
		}

		public void logout()
		{
			
		}

		public void logout_admin()
		{

		}

	    private void NavigateToMainPage(Frame currentFrame)
	    {
	        currentFrame.Navigate(typeof(MainPage));
        }

	    private void NavigateToAdminPage(Frame currentFrame)
	    {
	        currentFrame.Navigate(typeof(Admin));
	    }

		public static void NavigateToLoginPage(Frame currentFrame)
		{
			currentFrame.Navigate(typeof(Login));
		}

	}
}
