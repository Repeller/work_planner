﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WorkPlanner.Model;
using WorkPlanner.ViewModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace WorkPlanner.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AdminCreate : Page
    {
        //private readonly ViewModelAdminAdd _view = new ViewModelAdminAdd();

        public AdminCreate()
        {
            this.InitializeComponent();
            //DataContext = _view;
        }

        /// <summary>
        /// add one event, to an existing project. also cleans the SelectedEmployees
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_add_Click(object sender, RoutedEventArgs e)
        {
            SingleUser temp = SingleUser.User;

            temp.ProjectEventAdd(
                temp.Projects[listView_projects.SelectedIndex].Id, 
                DatePicker_Date.Date.DateTime, 
                TimePicker_StartTime.Time, 
                TimePicker_EndTime.Time, 
                temp.GetEmployeeList(), // this one xD
                TextBox_address.Text, 
                TextBox_Description.Text);

            temp.SelectedEmployees = new List<string>();
        }

        /// <summary>
        /// find out which employees that are selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_employees_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SingleUser tempMasterController = SingleUser.User;


            // add the list of selected employees
            foreach (Employee employee in e.AddedItems)
            {
                tempMasterController.SelectedEmployees.Add(employee.UserId);
            }

            // remove the list of unselected employees
            foreach (Employee employee in e.RemovedItems)
            {
                if (tempMasterController.SelectedEmployees.Contains(employee.UserId))
                {
                    tempMasterController.SelectedEmployees.Remove(employee.UserId);
                }
            }

        }

        private void Button_logout_Click(object sender, RoutedEventArgs e)
        {
            SingleUser temp = SingleUser.User;

            temp.Logout();
			ViewModelLogin.NavigateToLoginPage(Frame);
		}
    }
}
