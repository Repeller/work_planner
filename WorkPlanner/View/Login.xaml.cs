﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WorkPlanner.ViewModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace WorkPlanner.View
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class Login
	{
		private readonly ViewModelLogin  _view = new ViewModelLogin();

		public Login()
		{
			this.InitializeComponent();
			DataContext = _view;
		}


		private void Button_login_OnClick(object sender, RoutedEventArgs e)
		{
			_view.login(Frame);
		}

		private void Button_admin_OnClick(object sender, RoutedEventArgs e)
		{
			_view.login_admin(Frame);
		}
	}
}
