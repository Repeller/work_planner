﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
    public class BasicUser
    {
	    public string Username { get; }
	    public string Password { get; set; }
	    public string Name { get; set; }

	    public BasicUser(string username, string password, string name)
	    {
		    Username = username;
		    Password = password;
		    Name = name;
	    }

	    public override string ToString()
	    {
		    return $"{nameof(Name)}: {Name}";
	    }
    }
}
