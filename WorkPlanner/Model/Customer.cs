﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
	class Customer
	{
		static private int _idCounter;

		public int Id { get; set; }

		public string Name { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }

		public Customer(string name, string email, string phoneNumber)
		{
			_idCounter++;
			Id = _idCounter;

			Name = name;
			Email = email;
			PhoneNumber = phoneNumber;
		}

		public override string ToString()
		{
			return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(Email)}: {Email}, {nameof(PhoneNumber)}: {PhoneNumber}";
		}
	}
}
