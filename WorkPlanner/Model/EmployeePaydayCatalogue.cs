﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
	public class EmployeePaydayCatalogue
	{
		// public List<EmployeeWorkDays> Paydays { get; set; }
        public Dictionary<string, EmployeeWorkDays> PayDays { get; set; }

		public EmployeePaydayCatalogue()
		{
			PayDays = new Dictionary<string, EmployeeWorkDays>();
		}

	    public override string ToString()
	    {
	        return $"{nameof(PayDays)}: {PayDays}";
	    }
	}
}
