﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
	public class Resource
	{
		private static int ResourceCount;

		public int Id { get; set; }
		public string Name { get; set; }
		public string Amount { get; set; }
		public DateTime DateAdded { get; set; }
		public double Price { get; set; }
		public int ProjectId { get; set; }
		public int WorkerId { get; set; }

		public Resource(string name, string amount, DateTime dateAdded, double price, int projectId, int workerId)
		{
			ResourceCount++;
			Id = ResourceCount;
			Name = name;
			Amount = amount;
			DateAdded = dateAdded;
			Price = price;
			ProjectId = projectId;
			WorkerId = workerId;
		}
	}
}
