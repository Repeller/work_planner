﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
	public sealed class SingleUser
	{
		private SingleUser()
		{
			LoadEmployees();
			LoadProjects();
			LoadEmployeeCatalogue();
		}

		private static SingleUser _user = null;

		public static SingleUser User
		{
			get
			{
				if (_user == null)
				{
					_user = new SingleUser();
					return _user;
				}
				else
					return _user;
			}
		}

		// public WorkDay temp = new ()
		// 

		// this will be the info for the user, that is logged in
		public Employee LoggedEmployee = null;
		public Admin LoggedAdmin = null;

		public Day HoursOfTheDay = null;

		public List<Project> Projects = new List<Project>();
		public List<Employee> ListOfEmployees = new List<Employee>();
		
		public EmployeePaydayCatalogue EmployeePaychecks = new EmployeePaydayCatalogue();

        public List<string> SelectedEmployees = new List<string>();

        /// <summary>
        /// Loads some placeholder data, into the "EmployeePaychecks" object.
        /// </summary>
        public void LoadEmployeeCatalogue()
		{
			// this is the value, that will hold all the workers, that have checked in, on the days they should had worked.
		    EmployeePaydayCatalogue Days = new EmployeePaydayCatalogue();

			// the same pauses for all the workers
		    ObservableCollection<TimeRegistering> pauses = new ObservableCollection<TimeRegistering>();
            pauses.Add(new TimeRegistering(TimeRegistering.RegType.Pause, TimeRegistering.RegKind.Start, new DateTime(2018, 12, 3, 12, 0, 0)));
		    pauses.Add(new TimeRegistering(TimeRegistering.RegType.Pause, TimeRegistering.RegKind.End, new DateTime(2018, 12, 3, 12, 15, 0)));
		    pauses.Add(new TimeRegistering(TimeRegistering.RegType.Pause, TimeRegistering.RegKind.Start, new DateTime(2018, 12, 4, 13, 0, 0)));
		    pauses.Add(new TimeRegistering(TimeRegistering.RegType.Pause, TimeRegistering.RegKind.End, new DateTime(2018, 12, 4, 13, 15, 0)));

			// this a temp placeholder, where we will add the values to it, for each worker
		    EmployeeWorkDays TempWorkDay = new EmployeeWorkDays("UserId-1");

            #region user1

            // user 1

            TempWorkDay.WorkDays.Add(
                new Day("UserId-1",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 3, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 3, 15, 15, 0)), pauses,
                    Projects[0].ListOfScheduledEvents[0]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-1",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 4, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 4, 15, 15, 0)), pauses,
                    Projects[0].ListOfScheduledEvents[1]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-1",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 5, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 5, 15, 15, 0)), pauses,
                    Projects[0].ListOfScheduledEvents[2]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-1",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 6, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 6, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[0]));
            TempWorkDay.WorkDays.Add(
                new Day("UserId-1",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 7, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 7, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[1]));

            #endregion
		    Days.PayDays.Add("UserId-1", TempWorkDay);

            #region User 2

            // user 2
            TempWorkDay = new EmployeeWorkDays("UserId-2");
            TempWorkDay.WorkDays.Add(
                new Day("UserId-2",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 3, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 3, 15, 15, 0)), pauses,
                    Projects[0].ListOfScheduledEvents[0]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-2",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 4, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 4, 15, 15, 0)), pauses,
                    Projects[0].ListOfScheduledEvents[1]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-2",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 5, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 5, 15, 15, 0)), pauses,
                    Projects[0].ListOfScheduledEvents[2]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-2",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 6, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 6, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[0]));
            TempWorkDay.WorkDays.Add(
                new Day("UserId-2",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 7, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 7, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[1]));

            #endregion
            Days.PayDays.Add("UserId-2", TempWorkDay);

            #region user3
            // user 3
            TempWorkDay = new EmployeeWorkDays("UserId-3");

            TempWorkDay.WorkDays.Add(
                new Day("UserId-3",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 3, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 3, 15, 15, 0)), pauses,
                    Projects[1].ListOfScheduledEvents[0]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-3",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 4, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 4, 15, 15, 0)), pauses,
                    Projects[1].ListOfScheduledEvents[1]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-3",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 5, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 5, 15, 15, 0)), pauses,
                    Projects[1].ListOfScheduledEvents[2]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-3",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 6, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 6, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[0]));
            TempWorkDay.WorkDays.Add(
                new Day("UserId-3",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 7, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 7, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[1]));

            #endregion
            Days.PayDays.Add("UserId-3", TempWorkDay);

            #region user4

            // user 4

            TempWorkDay = new EmployeeWorkDays("UserId-4");

            TempWorkDay.WorkDays.Add(
                new Day("UserId-4",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 3, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 3, 15, 15, 0)), pauses,
                    Projects[1].ListOfScheduledEvents[0]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-4",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 4, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 4, 15, 15, 0)), pauses,
                    Projects[1].ListOfScheduledEvents[1]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-4",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 5, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 5, 15, 15, 0)), pauses,
                    Projects[1].ListOfScheduledEvents[2]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-4",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 6, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 6, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[0]));
            TempWorkDay.WorkDays.Add(
                new Day("UserId-4",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 7, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 7, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[1]));
            #endregion
		    Days.PayDays.Add("UserId-4", TempWorkDay);

            #region user5

            // user 5
            TempWorkDay = new EmployeeWorkDays("UserId-5");

            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 3, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 3, 15, 15, 0)), pauses,
                    Projects[2].ListOfScheduledEvents[0]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 4, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 4, 15, 15, 0)), pauses,
                    Projects[2].ListOfScheduledEvents[1]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 5, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 5, 15, 15, 0)), pauses,
                    Projects[2].ListOfScheduledEvents[2]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 6, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 6, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[0]));
            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 7, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 7, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[1]));

            #endregion
		    Days.PayDays.Add("UserId-5", TempWorkDay);

            #region user6
            // user 6

            TempWorkDay = new EmployeeWorkDays("UserId-6");

            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 3, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 3, 15, 15, 0)), pauses,
                    Projects[2].ListOfScheduledEvents[0]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 4, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 4, 15, 15, 0)), pauses,
                    Projects[2].ListOfScheduledEvents[1]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 5, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 5, 15, 15, 0)), pauses,
                    Projects[2].ListOfScheduledEvents[2]));

            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 6, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 6, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[0]));
            TempWorkDay.WorkDays.Add(
                new Day("UserId-6",
                    new TimeRegistering(
                        TimeRegistering.RegType.Work,
                        TimeRegistering.RegKind.Start,
                        new DateTime(2018, 12, 7, 8, 15, 0)),
                    new TimeRegistering(TimeRegistering.RegType.Work, TimeRegistering.RegKind.End,
                        new DateTime(2018, 12, 7, 15, 15, 0)), pauses,
                    Projects[3].ListOfScheduledEvents[1])); 
            #endregion
            Days.PayDays.Add("UserId-6", TempWorkDay);

		    EmployeePaychecks = Days;

		    //TimeRegistering.RegKind.Start, new DateTime(2018, 12, 3, 8, 15, 0));
		}

		public void LoadProjects()
		{
			// the employees that work on all the projects
			ObservableCollection<Employee> TempListOfEmployees = new ObservableCollection<Employee>();

			List<ScheduledEvent> listOfEvents = new List<ScheduledEvent>();
			///listOfEvents.Add(new ScheduledEvent(new DateTime(2000, 1, 1, 12,0,0), new DateTime(2000, 1, 1, 14,0,0), TempListOfEmployees, "hvorGade 123, 0404", "hard work, but good pay"));

			Project temProject;

			// for loop
			for (int i = 0; i < 4; i++)
			{
				switch (i)
				{
					case 0:
						temProject = new Project("paint the school", "hell 23, 6666", 1);
						TempListOfEmployees.Add(ListOfEmployees[0]);
						TempListOfEmployees.Add(ListOfEmployees[1]);
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 3, 8,0,0), new DateTime(2018, 12, 3, 15,0,0), TempListOfEmployees, "gadeNavn 20", "paint job" ));
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 4, 8, 0, 0), new DateTime(2018, 12, 4, 15, 0, 0), TempListOfEmployees, "gadeNavn 20", "paint job"));
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 5, 8, 0, 0), new DateTime(2018, 12, 5, 15, 0, 0), TempListOfEmployees, "gadeNavn 20", "paint job"));
						break;
					case 1:
						temProject = new Project("paint the ground bitch", "hell 23, 6666", 1);
						TempListOfEmployees.Add(ListOfEmployees[2]);
						TempListOfEmployees.Add(ListOfEmployees[3]);
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 3, 8, 0, 0), new DateTime(2018, 12, 3, 15, 0, 0), TempListOfEmployees, "Roskilde husvej 200", "paint job"));
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 4, 8, 0, 0), new DateTime(2018, 12, 4, 15, 0, 0), TempListOfEmployees, "Roskilde husvej 200", "paint job"));
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 5, 8, 0, 0), new DateTime(2018, 12, 5, 15, 0, 0), TempListOfEmployees, "Roskilde husvej 200", "paint job")); break;
					case 2:
						temProject = new Project("make a car out of wood", "gade vej 54, 5321", 1);
						TempListOfEmployees.Add(ListOfEmployees[4]);
						TempListOfEmployees.Add(ListOfEmployees[5]);
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 3, 8, 0, 0), new DateTime(2018, 12, 3, 15, 0, 0), TempListOfEmployees, "Roskilde bakkedal 45", "paint job"));
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 4, 8, 0, 0), new DateTime(2018, 12, 4, 15, 0, 0), TempListOfEmployees, "Roskilde bakkedal 45", "paint job"));
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 5, 8, 0, 0), new DateTime(2018, 12, 5, 15, 0, 0), TempListOfEmployees, "Roskilde bakkedal 45", "paint job"));
						break;
					case 3:
						temProject = new Project("make a boy out of wood", "USA vej 24, 1231", 1);
						TempListOfEmployees.Add(ListOfEmployees[0]);
						TempListOfEmployees.Add(ListOfEmployees[1]);
						TempListOfEmployees.Add(ListOfEmployees[2]);
						TempListOfEmployees.Add(ListOfEmployees[3]);
						TempListOfEmployees.Add(ListOfEmployees[4]);
						TempListOfEmployees.Add(ListOfEmployees[5]);
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 6, 8, 0, 0), new DateTime(2018, 12, 6, 15, 0, 0), TempListOfEmployees, "FakeName 69", "paint job")); 
						listOfEvents.Add(new ScheduledEvent(new DateTime(2018, 12, 7, 8, 0, 0), new DateTime(2018, 12, 7, 15, 0, 0), TempListOfEmployees, "FakeName 69", "paint job"));
						break;
					//case 4:
					//	temProject = new Project("make a girl out of wood", "superman vej 756, 2343", 1);
					//	break;
					//case 5:
					//	temProject = new Project("make a man out of wood", "batman 23, 4000", 1);
					//	break;
					//case 6:
					//	temProject = new Project("make a girl out of wood", "superman vej 756, 2343", 1);
					//	break;
					//case 7:
					//	temProject = new Project("make a woman out of wood", "bakkegade 245, 5661", 1);
					//	break;
					//case 8:
					//	temProject = new Project("make a duck out of wood", "gadenavn 254, 1742", 1);
					//	break;
					//case 9:
					//	temProject = new Project("make some kind of art, out of wood", "landevej 214, 1234", 1);
					//	break;
					//case 10:
					//	temProject = new Project("paint the city red", "roskilde, 4000", 1);
					//	break;
					//case 11:
					//	temProject = new Project("make a house, out of stone", "Roskilde vej 24, 4000", 1);
					//	break;
					default:
						temProject = new Project("this should never be showed", "you fucked up the code bitch, FIX IT", 1);
						break;
				}

				// add the temp values, to the list, so we can use them later.
				temProject.ListOfScheduledEvents = listOfEvents;
				Projects.Add(temProject);

				// clean the temp values, so we can use them in the next loop
				listOfEvents = new List<ScheduledEvent>();
				TempListOfEmployees = new ObservableCollection<Employee>();
			}		
		}

		/// <summary>
		/// loads the list of employees, so we know which users there are.
		/// </summary>
		public void LoadEmployees()
		{
			ListOfEmployees.Add(new Employee("user1", "password", "Kim", "1234 5678", "email@email.com"));
			ListOfEmployees.Add(new Employee("user2", "password", "Bil", "1234 5678", "email@email.com"));
			ListOfEmployees.Add(new Employee("user3", "password", "Bob", "1234 5678", "email@email.com"));
			ListOfEmployees.Add(new Employee("user4", "password", "Bo", "1234 5678", "email@email.com"));
			ListOfEmployees.Add(new Employee("user5", "password", "Alex", "1234 5678", "email@email.com"));
			ListOfEmployees.Add(new Employee("user6", "password", "Tina", "1234 5678", "email@email.com"));
		}

		/// <summary>
		/// this will check the input and assigned the Employee (if it exist) to 'LoggedEmployee'.
		/// </summary>
		/// <param name="username">the username input</param>
		/// <param name="password">the password input</param>
		/// <returns>returns true if it found an Employee, returns false if it didn't find the Employee</returns>
		public bool checkEmployeeInput(string username, string password)
		{
			//Employee tempUser = EmployeeListLogin(username, password);

			foreach (Employee employee in ListOfEmployees)
			{
				if(employee.Username == username && employee.Password == password)
				{
					LoggedEmployee = employee;
					return true;
				}
			}

			// HoursOfTheDay = new Day(tempUser.UserId);
			return false;
		}

		///// <summary>
		///// this is the list of users, that are used when some one tries to login
		///// </summary>
		///// <param name="username">the username input</param>
		///// <param name="password">the password input</param>
		///// <returns>return the Employee. If there doesn't exist a user with those values, an Employee with 'error' as all values will be returned</returns>
		//public void EmployeeListLogin(string username, string password)
		//{
		//	List<Employee> userList = new List<Employee>();
		//	Employee defaultUser = new Employee("error", "error", "error", "error", "error");
			
		//	userList.Add(new Employee("user1", "password", "Kim", "1234 5678", "email@email.com"));
		//	userList.Add(new Employee("user2", "password", "Bil", "1234 5678", "email@email.com"));
		//	userList.Add(new Employee("user3", "password", "Bob", "1234 5678", "email@email.com"));
		//	userList.Add(new Employee("user4", "password", "Bo", "1234 5678", "email@email.com"));
		//	userList.Add(new Employee("user5", "password", "Alex", "1234 5678", "email@email.com"));
		//	userList.Add(new Employee("user6", "password", "Tina", "1234 5678", "email@email.com"));

		//	foreach (Employee user in userList)
		//	{
		//		if (user.Username == username && user.Password == password)
		//			return user;
		//	}

		//	return defaultUser;
		//}

		/// <summary>
		/// checks the input of the admin login. will return false if it doesn't exist. the 'loggedAdmin' will be assigned to the right admin
		/// </summary>
		/// <param name="username">the username input</param>
		/// <param name="password">the password input</param>
		/// <returns>true if the admin exist, false if it doesn't</returns>
		public bool CheckAdminInput(string username, string password)
		{
			Admin tempAdmin = AdminListLogin(username, password);

			if (tempAdmin.Username == "error")
				return false;
			else
			{
				LoggedAdmin = tempAdmin;
				return true;
			}
		}

		/// <summary>
		/// this is the list of all the admin login data. it is used to check if the admin used the right login inputs
		/// </summary>
		/// <param name="username">username input</param>
		/// <param name="password">password input</param>
		/// <returns>return the right admin, or one with error on all the values, if it doesn't exist</returns>
		public Admin AdminListLogin(string username, string password)
		{
			List<Admin> admins = new List<Admin>();

			admins.Add(new Admin("admin1", "password", "big kim"));
			admins.Add(new Admin("admin2", "password", "big bob"));
			admins.Add(new Admin("admin3", "password", "small Lars"));

			foreach (Admin admin in admins)
			{
				if (admin.Username == username && admin.Password == password)
				{
					return admin;
				}
			}

			return new Admin("error", "error", "error");
		}

		/// <summary>
		/// set the loggedAdmin value to null
		/// </summary>
		public void AdminLogout()
		{
			
		}

        /// <summary>
        /// get a list of employees, that are selected on the admin page
        /// </summary>
        /// <returns>the list of employees that are selected</returns>
	    public ObservableCollection<Employee> GetEmployeeList()
	    {
		    ObservableCollection<Employee> employees = new ObservableCollection<Employee>();

	        foreach (string selectedEmployee in SelectedEmployees)
	        {
	            employees.Add(ListOfEmployees.Find(x => x.UserId == selectedEmployee));
	        }

	        return employees;
	    }

        /// <summary>
        /// set the loggedUser and the LoggedAdmin value to null
        /// </summary>
        public void Logout()
		{
			LoggedEmployee = null;
		    LoggedAdmin = null;
        }


        #region Admin Project methods

        /// <summary>
        /// adds one project to the project list
        /// </summary>
        /// <param name="newProject">the project object that will be added</param>
	    public void ProjectAdd(Project newProject)
	    {
            Projects.Add(newProject);
	    }

	    public void ProjectDelete()
	    {

	    }

	    public void ProjectEdit()
	    {

	    }

        // events

        /// <summary>
        /// add one event to an existing project
        /// </summary>
        /// <param name="projectId">the id of the project</param>
        /// <param name="date">the date of the event</param>
        /// <param name="start">the start time of the event</param>
        /// <param name="end">the end time of the event</param>
        /// <param name="workers">the list of workers, that will be working on this event</param>
        /// <param name="address">the address of this event</param>
        /// <param name="description">the description to the event</param>
	    public void ProjectEventAdd(int projectId, DateTime date, TimeSpan start, TimeSpan end, ObservableCollection<Employee> workers, string address, string description)
	    {
            Projects.Find(x => x.Id == projectId).ListOfScheduledEvents.Add(
                new ScheduledEvent(new DateTime(date.Year, date.Month, date.Day, start.Hours, start.Minutes, 0),
	            new DateTime(date.Year, date.Month, date.Day, end.Hours, end.Minutes, 0), workers, address, description));
        }

	    /// <summary>
	    /// removes one existing event, from an existing project 
	    /// </summary>
	    /// <param name="projectId">the id of the project</param>
        /// <param name="eventId">the id of the event</param>
        public void ProjectEventDelete(int projectId, int eventId)
        {
            int projectIndex = Projects.IndexOf(Projects.Find(x => x.Id == projectId));

            Projects[projectIndex].ListOfScheduledEvents.Remove(
                Projects[projectIndex].ListOfScheduledEvents.Find(x => x.Id == eventId));
        }

        /// <summary>
        /// edit an existing event, from an existing project. All the input don't need to be new values, just insert the same values of the old one
        /// </summary>
        /// <param name="projectId">the id of the project</param>
        /// <param name="eventId">the event of the event, that will be edited</param>
        /// <param name="newDate">the new date</param>
        /// <param name="newStart">the new start time</param>
        /// <param name="newEnd">the new end time</param>
        /// <param name="newWorkers">the new list of the workers</param>
        /// <param name="newAddress">the new address</param>
        /// <param name="newDescription">the new description</param>
        public void ProjectEventEdit(int projectId, int eventId, DateTime newDate, TimeSpan newStart, TimeSpan newEnd, ObservableCollection<Employee> newWorkers, string newAddress, string newDescription)
        {
            int EventIndex = Projects.Find(x => x.Id == projectId).ListOfScheduledEvents.FindIndex(x => x.Id == eventId);

            Projects.Find(x => x.Id == projectId).ListOfScheduledEvents[EventIndex] = 
                new ScheduledEvent(new DateTime(newDate.Year, newDate.Month, newDate.Day, newStart.Hours, newStart.Minutes, 0),
                    new DateTime(newDate.Year, newDate.Month, newDate.Day, newEnd.Hours, newEnd.Minutes, 0), newWorkers, newAddress, newDescription);
        }
            
        #endregion

	    #region Admin Hour methods

	    //public void HoursEdit()
	    //{

	    //}

	    //public void HoursDelete()
	    //{

	    //}

	    #endregion
    }
}
