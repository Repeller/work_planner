﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
	public class TimeRegistering
	{
		private static int IdCount;

		public int Id { get; set; }
		public enum RegType { Pause, Work, Other }
		public enum RegKind { Start, End }

		public RegType TypeInput { get; set; }
		public RegKind KindInput { get; set; }

		public DateTime TimeAndDate { get; set; }

		public TimeRegistering(RegType typeInput, RegKind kindInput, DateTime timeAndDate)
		{
			IdCount++;
			Id = IdCount;
			TypeInput = typeInput;
			KindInput = kindInput;
			TimeAndDate = timeAndDate;
		}
	    public override string ToString()
	    {
	        return $"type: {TypeInput}, when: {KindInput}, date and time: {TimeAndDate}";
	    }
	}
}

