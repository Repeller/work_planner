﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
    public class Admin : BasicUser
    {
	    private static int AdminCount;

	    public string AdminId { get; }

	    public Admin(string username, string password, string name) : base (username, password, name)
	    {
		    AdminCount++;
		    AdminId = "AdminId-" + AdminCount;
	    }
    }
}
