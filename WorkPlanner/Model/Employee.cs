﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
	public class Employee : BasicUser
	{
		private static int EmployeeCount;

		public string UserId { get; set; }
		public string PhoneNumber { get; set; }
		public string Email { get; set; }

		protected bool Equals(Employee other)
		{
			return string.Equals(UserId, other.UserId);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Employee) obj);
		}

		public override int GetHashCode()
		{
			return (UserId != null ? UserId.GetHashCode() : 0);
		}

		public Employee(string username, string password, string name, string phoneNumber, string email) : base(username, password, name)
		{
			EmployeeCount++;
			UserId = "UserId-" + EmployeeCount;
			PhoneNumber = phoneNumber;
			Email = email;
		}
	}
}
