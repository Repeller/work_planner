﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
	public class EmployeeWorkDays
	{
		private static int IdCounter;
		public int Id { get; set; }

        public string EmployeeId { get; }
		public List<Day> WorkDays { get; set; }
		//public Dictionary<string, Day> WorkDays { get; set; }

		public TimeSpan PauseTimeCombined
		{
			get
			{
				TimeSpan AllPauseTime = new TimeSpan();

				foreach (Day day in WorkDays)
				{
					AllPauseTime.Add(day.TotalPauseTime);
				}

				return AllPauseTime;
			}
		}

		public TimeSpan WorkHoursCombined
		{
			get
			{
				TimeSpan AllWorkHours = new TimeSpan();

				foreach (Day day in WorkDays)
				{
					AllWorkHours.Add(day.TotalWorkHours);
				}

				return AllWorkHours;
			}
		}

		public TimeSpan OverTimeCombined
		{
			get
			{
				TimeSpan AllOvertime = new TimeSpan();

				foreach (Day day in WorkDays)
				{
					AllOvertime.Add(day.TotalOvertime);
				}

				return AllOvertime;
			}
		}


		public EmployeeWorkDays(string employeeId)
		{
			IdCounter++;
		    EmployeeId = employeeId;
			Id = IdCounter;
			WorkDays = new List<Day>();
		}

	    public override string ToString()
	    {
	        return $"{nameof(Id)}: {Id}, {nameof(EmployeeId)}: {EmployeeId}, {nameof(WorkDays)}: {WorkDays}";
	    }
	}
}
