﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
	public class ScheduledEvent
	{
		private static int ScheduledEventCount;

		public int Id { get; set; }
		public string Address { get; set; }
		public string Description { get; set; }

		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public ObservableCollection<Employee> ListOfEmployees { get; set; }

		public ScheduledEvent(DateTime start, DateTime end, ObservableCollection<Employee> listOfEmployees, string address, string description)
		{
			ScheduledEventCount++;
			Id = ScheduledEventCount;
			Start = start;
			End = end;
			ListOfEmployees = listOfEmployees;

			Address = address;
			Description = description;
		}

		public override string ToString()
		{
			return $"{nameof(Id)}: {Id}, {nameof(Address)}: {Address}, {nameof(Description)}: {Description}, {nameof(Start)}: {Start}, {nameof(End)}: {End}, {nameof(ListOfEmployees)}: {ListOfEmployees}";
		}
	}
}
