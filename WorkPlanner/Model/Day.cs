﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WorkPlanner.Model
{
	public class Day
	{
		private static int IdCounter;

		public int Id { get; set; }

		public TimeRegistering Start { get; set; }
		public TimeRegistering End { get; set; }

		public ObservableCollection<TimeRegistering> Pauses { get; set; }
		public ScheduledEvent ScheduledHours { get; }

		public string EmployeeId { get; set; }

		public TimeSpan TotalWorkHours => End.TimeAndDate.TimeOfDay.Subtract(Start.TimeAndDate.TimeOfDay);

		public TimeSpan TotalPauseTime
		{
			get
			{
				var tempTimeSpan = new TimeSpan();

				var startPauseTempValue = new TimeSpan();


				foreach (var pause in Pauses)
				{
					if (pause.KindInput == TimeRegistering.RegKind.Start)
						startPauseTempValue = pause.TimeAndDate.TimeOfDay;

					if (pause.KindInput == TimeRegistering.RegKind.End)
					{
						var tempSpan = pause.TimeAndDate.TimeOfDay;
						var tempSpan2 = startPauseTempValue;
						var tempSpan3 = new TimeSpan();

						tempSpan3 = tempSpan.Subtract(tempSpan2);

						tempTimeSpan.Add(tempSpan3);
					}
				}

				return tempTimeSpan;
			}
		}

		public TimeSpan TotalOvertime
		{
			get
			{
				var tempTimeSpan = new TimeSpan();

				var PlanedTimeEnd = ScheduledHours.End.TimeOfDay;
				var RegTimeEnd = End.TimeAndDate.TimeOfDay;

				var overtime = RegTimeEnd.Subtract(PlanedTimeEnd);

				if (overtime > new TimeSpan(0, 0, 0, 0))
					tempTimeSpan = overtime;

				return tempTimeSpan;
			}
		}

		public Day(string userId, ScheduledEvent scheduledHours)
		{
			IdCounter++;
			Id = IdCounter;
			EmployeeId = userId;
			Pauses = new ObservableCollection<TimeRegistering>();
			ScheduledHours = scheduledHours;
		}

		public Day(string userId, TimeRegistering start, TimeRegistering end, ObservableCollection<TimeRegistering> pauses,
			ScheduledEvent scheduledHours)
		{
			IdCounter++;
			Id = IdCounter;
			EmployeeId = userId;
			Pauses = pauses;
			Start = start;
			End = end;
			ScheduledHours = scheduledHours;
		}

		/// <summary>
		///     return the amount of hours that a employee worked through the day
		/// </summary>
		/// <returns>hours that the employee worked in a day</returns>
		public string PayCalculator()
		{
			var tempDateTime = new DateTime();
			var minus = Start.TimeAndDate.TimeOfDay;

			tempDateTime = End.TimeAndDate.Subtract(minus);

			tempDateTime.Subtract(PauseTime());

			return tempDateTime.ToString();
		}

		/// <summary>
		///     get the amount of time that a employee was on break
		/// </summary>
		/// <returns>the break time the employee have taken</returns>
		public TimeSpan PauseTime()
		{
			// add the first(start), remove the second (end)
			var tempTimeSpan = new TimeSpan(0, 0, 0, 0);

			var IsItStart = true;

			foreach (var pause in Pauses)
				if (pause.KindInput == TimeRegistering.RegKind.Start && IsItStart)
				{
					//tempTimeSpan.Add(new TimeSpan(0, pause.TimeAndDate.Hour, pause.TimeAndDate.Minute,
					//	pause.TimeAndDate.Second));
					tempTimeSpan.Add(pause.TimeAndDate.TimeOfDay);

					IsItStart = false;
				}
				else if (pause.KindInput == TimeRegistering.RegKind.End && !IsItStart)
				{
					//tempTimeSpan.Subtract(new TimeSpan(0, pause.TimeAndDate.Hour, pause.TimeAndDate.Minute,
					//	pause.TimeAndDate.Second));
					tempTimeSpan.Subtract(pause.TimeAndDate.TimeOfDay);
					IsItStart = true;
				}

			return tempTimeSpan;
		}
	}
}