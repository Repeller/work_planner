﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace WorkPlanner.Model
{
	public class Project
	{
		private static int ProjectCount = 0;

		public int Id { get; set; }

		public List<ScheduledEvent> ListOfScheduledEvents { get; set; }
		public List<Resource> Resources { get; set; }

		public string Description { get; set; }
		public string Address { get; set; }
		public int CustomerId { get; set; }

		public Project(string description, string address, int customerId)
		{
			ProjectCount++;
			Resources = new List<Resource>();
			ListOfScheduledEvents = new List<ScheduledEvent>();
			Description = description;
			Address = address;
		    Id = ProjectCount;
		    //CustomerId = customerId;
		}

        /// <summary>
        /// get the list of ScheduledEvents, that have the employee on them
        /// </summary>
        /// <returns></returns>
	    public ObservableCollection<ScheduledEvent> GetEventsWithThatEmployee()
	    {

			#region old way, that didn't work
			//ObservableCollection<ScheduledEvent> tempEvents = new ObservableCollection<ScheduledEvent>();

			//SingleUser tempMaster = SingleUser.User;

			//foreach (ScheduledEvent scheduledEvent in ListOfScheduledEvents)
			//{
			//	if (scheduledEvent.ListOfEmployees.Any(x => x.UserId == tempMaster.LoggedEmployee.UserId))
			//	{
			//		tempEvents.Add(scheduledEvent);

			//	}
			//}

			//return tempEvents; 
			#endregion

			SingleUser tempMaster = SingleUser.User;

			ObservableCollection<ScheduledEvent> tempCollection = new ObservableCollection<ScheduledEvent>();

			foreach (ScheduledEvent scheduledEvent in ListOfScheduledEvents)
		    {
			    foreach (Employee employee in scheduledEvent.ListOfEmployees)
			    {
				    if (employee.UserId == tempMaster.LoggedEmployee.UserId)
				    {
						tempCollection.Add(scheduledEvent);
				    }
			    }
		    }

		    return tempCollection;
	    }

        public override string ToString()
	    {
	        return $"{Description}";
	    }
	}
}